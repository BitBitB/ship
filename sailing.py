'''Describe what this module does.'''

import sys
import ship_actions as sa

#  Global variable
sailing_ship = {
    'name': 'Titanic',
    'max_nb_passengers': 34,
    'passengers_list': []
}


def main():
    ''' '''
    current_nb_of_passengers = len(sailing_ship["passengers_list"])
    print("There are {} passengers on the ship.".format(current_nb_of_passengers))
    try: 
        sa.clean_the_ship()
        sa.accept_passengers()
        sa.sail_away()
    except OrderShipException as e:
        print(ose)
    except Exception as e:
        print(e)
    finally:
        sys.exit(0)
    

if __name__ == '__main__':    
    main()
