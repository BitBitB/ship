import os


def foo_bar():
    pass

def main():
    try:
        foo_bar()
    except Exception as e:
        print(e)

if __name__ == '__main__':    
    main()
