PI = 3.1459

sailing_ship = {
    'name': 'Kitty Hawk',
    'max_nb_passengers': 3,
    'passengers_list': []
    }

class OrderShipException(Exception):
    pass


def clean_the_ship():
   print("Let's clean the ship")

   
def check_in_passenger(passenger):
    pl = sailing_ship['passengers_list']
    if len(pl) <= sailing_ship['max_nb_passengers']:
            print("Welcome on board ", passenger)
            pl.append(passenger)
    else:
        raise Exception("No space left on the ship")

    
def checkin_passengers(*args):
    for passenger in args:
        print("Checking in passenger ", passenger)
        try:
            check_in_passenger(passenger)
        except Exception as e:
            # log the exception 
            raise OrderShipException("Please order a new ship")

            
def accept_passengers():
   print("Welcome passengers")

   
def sail_away():
   print("Let's sail!")

def main():
    '''Test the functions in the module.'''
    
    checkin_passengers("John", "Eric", "Terry", "The rabit", "The parrot")
    
if __name__ == '__main__':    
    main()
